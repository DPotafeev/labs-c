/* ������� ���������� ������*/
#include "CompressHeadFile.h"

struct SYM* buildTree (struct SYM *psym[], int N)
{
	int n;
	struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));    //��������� ����
	temp->freq=psym[N-2]->freq + psym[N-1]->freq;                //� ���� ������� ���������� ���� ������������ ����� ������ ��������� 2� ���������
	temp->left=psym[N-1];										 // ��������� ���� � ����� ����������
	temp->right=psym[N-2]; 
	temp->code[0]=0;
	if(N==2)                                                    // �������� �������� ������� � �������� 1
		if(psym[N-1]->freq<=psym[N-2]->freq)					//�������, ����� ��� ����, ����� ����� ������ ������ ������� � ��� 0 
		return temp;											//
		else
		{
			temp->left=psym[N-2];
			temp->right=psym[N-1];
			temp->code[0]=0;
			return temp;
		}
	n=N-2;
	while(n>0)
	{
		if (temp->freq>psym[n]->freq)
		{
			psym[n+1]=psym[n];
			psym[n]=temp;
		}
		else break;
		--n;
	}
	return buildTree(psym,N-1);
}
