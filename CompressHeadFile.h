#include <stdio.h>
#ifndef COMPRESSHEADFILE_H
#define COMPRESSHEADFILE_H
#define MAX_SIZE 256

struct SYM // ������������� �������
{
	unsigned char ch;	// symbol
	float freq;		   // frequence symbol
	char code[MAX_SIZE];   // for a new code
	struct SYM *left;
	struct SYM *right;
};

union CODE												// ����������� ��� �������� ����������� ����				
{
	unsigned char ch;
	struct 
	{
		unsigned short b1:1;
		unsigned short b2:1;
		unsigned short b3:1;
		unsigned short b4:1;
		unsigned short b5:1;
		unsigned short b6:1;
		unsigned short b7:1;
		unsigned short b8:1;
	} byte;
};



int analys (FILE* , struct SYM *) ;		// ����������� ���� � ��������� ������ �������� � ����������� �� �������

struct SYM* buildTree (struct SYM* , int );		//������ ������

void makeCodes(struct SYM * );		// ��������� ���� 0 � 1

void Coding(FILE* ,struct SYM * ,int , FILE* );			// ������� ���� .101

unsigned char pack(unsigned char *);		// ��������� ����������� ���� 

#endif 
