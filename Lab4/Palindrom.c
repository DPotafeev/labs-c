//Lab4 Task 3 - Palindrom
#include <stdio.h>
#include <string.h>
#define SIZE 256

int main ()
{
	char string[SIZE];	
	int i=0,len=0,palindrom=0;

	puts("Enter the string");
	fgets(string,SIZE,stdin);

	string[strlen(string)-1]=0;
	len=strlen(string);

	for (i=0;i<(len/2);i++)
		{
			char *p=string+i;
			char *q=string+(len-1-i);
			if((*p)!=(*q))
			{
				puts("Wrong");
				break;
			}
			else
				palindrom++;
		}
	if (palindrom > 0)
		printf("It's a palindrom\n");
	return 0;
}