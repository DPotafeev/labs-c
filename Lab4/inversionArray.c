#include <stdio.h>
#define SIZE 256

int main ()
{
	char string[SIZE];
	char* word[SIZE];
	int i=0,j=0,inWord=0,wcount=0;
	
	puts("Enter the string");
	fgets(string,SIZE,stdin);

	while (string[i])
	{
		if(string[i]!=' ' && inWord==0)
		{
			inWord=1;
			wcount++;
			word[j++]=string+i;
		}
		else if (string[i]==' ' && inWord==1)
			inWord=0;

		i++;
	}
	for(j=wcount-1;j>=0;j--)
	{
		char *p=word[j];
		while (*p && *p!=' ')
		{
			printf("%c",*p++);
		}
		putchar(' ');
	}
	putchar('\n');
	return 0;
}