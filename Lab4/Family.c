#include <stdio.h>
#define SIZE 10
#define MAXSIZE 100

int main()
{
	int count=0,i=0,j=0,min_age,max_age;
	char name[SIZE][MAXSIZE];
	char *young,*old;
	int age[SIZE];

	printf("Enter the number of your family members\n");
	scanf("%d",&count);

	for(i=0;i<count;i++)
	{
		printf("Enter name and age of your %d member\n",i+1);
		scanf("%s %d",&name[i],&age[i]);
	}
	min_age=max_age=age[0];
	young=old=name;

	for(i=0;i<count;i++)
	{
		for(j=0;j<count;j++)
		{
			if(age[i]>age[j] && age[i]>max_age)
			{
				max_age=age[i];
				old=name[i];
			}
			else if(age[i]<age[j] && age[i]<min_age)
			{
				min_age=age[i];
				young=name[i];
			}
		}
	}
	printf("The most older in family is %s , age is %d\n",old,max_age);
	printf("The most youner in family is %s , age is %d\n",young,min_age);
	return 0;
}