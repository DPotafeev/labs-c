#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CompressHeadFile.h"

int main()
{	
	int ch;					// ������ ���� � �������������� �����
	int i=0,j=0,count=0;
	int size_of_101=0;			//������ ����� .101
	int full_size_of_101=0;			// ������ ����� .101 + ������������ ���-�� ��� �� �������, �������� 8
	unsigned char buf[8];
	struct SYM syms[MAX_SIZE];
	struct SYM *p[MAX_SIZE];
    struct SYM *root;
	FILE *f_101;
	FILE *result;
	char const *headsig = "HAFF";		// ��� ��������� ���������
	char *ext = "txt";					// ���������� ������������� �����

	FILE *fin=fopen("1.txt","rb");
	count=analys(fin,syms);			//����� �����������
	fclose(fin);
	for (i=0;i<count;i++)
		p[i]=&syms[i];
	root=buildTree(p,count);		// ��������� ������
	makeCodes(root);		// �������� � ������ ���� 0 � 1

	fin=fopen("1.txt","rb");
	f_101=fopen("1.101","wb");
	Coding(fin,syms,count,f_101);		//������� ���� .101 � ���������� �����
	fclose(fin);
	fclose(f_101);
	f_101=fopen("1.101","rb");
	fseek(f_101,0,SEEK_END);
	
	result=fopen("result.niit","wb");
	//������ ��������� � �������������� ����
	fwrite(headsig,1,strlen(headsig),result);		//��������� ��������� � ������ ��������������� �����
	fwrite(&count,sizeof (count), 1, result);		//��������� ���-�� ���������� ��������
	for(i=0;i<count;i++)
	{
		fwrite(&syms[i].ch,sizeof (char), 1, result);
		fwrite(&syms[i].freq, sizeof (float), 1, result);
	}
	size_of_101=ftell(f_101);				//������ ����� .101
	if (size_of_101 % 8)
	{
		i=8-(size_of_101 % 8);					// ��� ������ "������", ���� ������ .101 �� ������ 8
		full_size_of_101=size_of_101+8-(size_of_101 % 8);
	}
	else
	{
		i=0;
		full_size_of_101=size_of_101;
	}
	fwrite(&i,sizeof(int),1,result);			//����������  ����� "������"
	fwrite(&full_size_of_101, sizeof(int), 1, result);				// ���������� ������ �����
	fwrite(ext,sizeof(char), 3 , result);			// ����c����� ���������� ��������� �����

	for(i=0;i<full_size_of_101;i=i+8)				// ���������� �� .101 �  �������������� 
	{
		j=0;
		while (j<8)
		{
			if(i+j<=size_of_101)
				buf[j]=fgetc(f_101);
			else
				buf[j]='0';
			j++;
		}
		fputc(pack(buf),result);
	}
	fclose(result);
	fclose(f_101);
	
	for (i=0;i<count;i++)
		printf("Symbol %c	Frequence  %f	Code  %s\n",syms[i].ch,syms[i].freq,syms[i].code);     // ������ �������
	printf("\n");
	return 0;
}