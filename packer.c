#include "CompressHeadFile.h"

unsigned char pack(unsigned char buf[])
{
	union CODE code;
	code.byte.b1=buf[0]-'0';
	code.byte.b2=buf[1]-'0';
	code.byte.b3=buf[2]-'0';
	code.byte.b4=buf[3]-'0';
	code.byte.b5=buf[4]-'0';
	code.byte.b6=buf[5]-'0';
	code.byte.b7=buf[6]-'0';
	code.byte.b8=buf[7]-'0';
	return code.ch;
}