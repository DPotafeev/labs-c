//Lab2Task7
//table of occuerence symbols in string
#include <stdio.h>
#include <string.h>
#define MAX_SIZE 256

int main()
{
	int i,j,len,count,flag=0;			//flag - ����������, ��� �� ������ ���
	char str[MAX_SIZE],symbols[MAX_SIZE];
	int freq[MAX_SIZE];

	do
	{
		printf("Enter your string\n");
		fgets(str,MAX_SIZE,stdin);
		if (!(strlen(str)-1))
			printf("Error!!! String is empty! Enter new string!\n");
	}
	while (!(strlen(str)-1));		//end do-while
	len=strlen(str)-1;
	count=0;
	for(i=0;i<len;i++)
	{
		flag=0;
		for(j=0;j<count;j++)
		{
			if(str[i]==symbols[j])
			{
				flag=1;
				freq[j]++;
			}//end if
		}//end for
		if(flag==0)
		{
			count++;
			symbols[count-1]=str[i];
			freq[count-1]=1;
		}//end if
	}//end for
	for(i=0;i<count;i++)
		printf("Sym %2c		Freq  %2d\n",symbols[i],freq[i]);
	return 0;
}