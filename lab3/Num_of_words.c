#include <stdio.h>
#include <string.h>

int main()
{
	int i=0,wcount=0,inWord=0;
	char buf[80];
	puts("Enter a string: ");
	fgets(buf,80,stdin);
	buf[strlen(buf)-1]=0;

	while (buf[i])
	{
		if (buf[i]!=' ' && inWord==0)
		{
			inWord=1;
			wcount++;
		}
		else if (buf[i]==' ' && inWord==1)
			inWord=0;
		i++;
	}
	printf("Words %d\n",wcount);
	return 0;
}