#include <stdio.h>
#include <ctype.h>
#define SIZE 256

int main()
{
	int i,n,count=0,inword=0,len,numofwords=0,flag=0;
	char str[SIZE];

	printf("Enter the string\n");
	fgets(str,SIZE,stdin);
	str[strlen(str)-1]='\0';
	len=strlen(str);
	for(i=0;i<len;i++)
	{
		if(isalnum(str[i]) && inword==0)
		{
			inword=1;
			count++;
		}
		else if(isspace(str[i]) && inword==1)
			inword=0;
	}
	do
	{
		puts("Enter the number of word");
		scanf("%d",&n);
		if(n<1 || n>count)
			puts("Error!! Wrong number!");
	}
	while (n<1 || n >count);
	inword=0;
	numofwords=n;
	for(i=0;numofwords>0,i<len;i++)
	{
		if(isalnum(str[i]) && inword==0 && numofwords>1)
		{
			inword=1;
		}
		else if (isspace(str[i]) && inword==1 && numofwords>1)
		{
			numofwords--;
			inword=0;
		}
		else if (isalnum(str[i]) && inword==0 && numofwords==1)
		{
			flag=1;
			putchar(str[i]);
		}
		else if (isspace(str[i]) && flag==1 &&  numofwords==1)
		{
			numofwords--;
			inword=0;
		}
	}
	printf("  - this is the %d word in your string!\n",n);


	return 0;
}