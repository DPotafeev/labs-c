#include <stdio.h>
#include <ctype.h>
#define SIZE 256

int main()
{
	int i,j=0,n,count=0,inword=0,len,numofwords=0,value=0;
	char str[SIZE];

	printf("Enter the string\n");
	fgets(str,SIZE,stdin);
	len=strlen(str)-1;
	for(i=0;i<len;i++)
	{
		if(isalnum(str[i]) && inword==0)
		{
			inword=1;
			count++;
		}
		else if(isspace(str[i]) && inword==1)
			inword=0;
	}
	do
	{
		puts("Enter the number of word");
		scanf("%d",&n);
		if(n<1 || n>count)
			puts("Error!! Wrong number!");
	}
	while (n<1 || n >count);
	inword=0;
	numofwords=n;
	for(i=0;i<len;i++)
	{
		if(isalnum(str[i]) && inword==0 && numofwords>1)
		{
			inword=1;
		}
		else if (isspace(str[i]) && inword==1 && numofwords>1)
		{
			numofwords--;
			inword=0;
		}
		else if (isalnum(str[i]) && inword==0 && numofwords==1)
		{
			value++;
		}
		else if (isspace(str[i]) && value>0 &&  numofwords==1)
		{
			numofwords--;
			inword=0;
			for(j=value;j>0;j--)
			{
				str[i-j]='\0';
			}
		}
	}
	if(numofwords==1 && value>0)
	{
		for(j=value;j>0;j--)
			{
				str[i-j]='\0';
			}
	}
	i=0;
	puts("The new string is");
	while(str[i]!='\n')
		putchar(str[i++]);
	putchar('\n');
	return 0;
}