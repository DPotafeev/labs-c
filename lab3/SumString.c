#include <stdio.h>
#define SIZE 80
#define MAXSIZEVALUE 3

int main()
{
	char str[SIZE];
	char *p=str;
	int i=0,j=0,sum=0,result=0;
	puts("Enter the string");
	fgets(str,SIZE,stdin);

	while(*p)
	{
		if(*p>='0' && *p<='9')
		{
			if (i<MAXSIZEVALUE)
			{
				i++;
				sum=sum*10+*p-'0';
				*p++;
			}
			else
			{
				i=1;
				result=result+sum;
				sum=*p -'0';
				*p++;
			}
		}
		else 
		{
			i=0;
			result=result+sum;
			sum=0;
			*p++;
		}

	}
		if(sum>0)
			result=result+sum;
		

		printf("Sum of values in the string is : %d\n", result);
		return 0;
}